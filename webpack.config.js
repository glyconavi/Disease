let path = require("path");
let fileName = "js/main.js";
let src = path.resolve(__dirname, "src");
let dist = path.resolve(__dirname, "dist");
let HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: {
        app: path.join(src, fileName)
    },
    output: {
        path: dist,
        filename: "[name].bundle.js"
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                //BabelでJSコードをES2015+ -> ES5変換
                test: /\.js$/,
                exclude: /node_modules | bower_components/,
                loader: "babel-loader"
            },
            {
                test: /\.css$/,
                loader: "style-loader!css"
            },
            {
                // 追記
                test: /\.(jpg|png|svg)$/,
                loaders: "file-loader?name=[name].[ext]",
                options: {
                    esModule: false
                }
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(src, "index.html"),
            filename: "index.html"
        })
    ]
};


