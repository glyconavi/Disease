//@flow

"use strict";

//https://pubmed.ncbi.nlm.nih.gov/24256719/{PubMedID}/
export const pubmed = {
    URL: "https://pubmed.ncbi.nlm.nih.gov/"
};

//https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&retmode=json&id={PumMedID}
export const pubmedapi = {
    URL: "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi"
};

//https://dx.doi.org/{doi}
export const doi = {
    URL: "https://dx.doi.org/"
};

//https://api.crossref.org/works/{doi}
export const crossrefapi = {
    URL: "https://api.crossref.org/works/"
};