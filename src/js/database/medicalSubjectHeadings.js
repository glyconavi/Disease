//@flow

"use strict";

//https://meshb.nlm.nih.gov/record/ui?ui={MeSHID}
export const mesh = {
    URL: "https://meshb.nlm.nih.gov/record/ui?ui="
};