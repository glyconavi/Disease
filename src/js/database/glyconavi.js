//@flow

"use strict";

//https://test.glyconavi.org/GlycoAbun/Sample/index.php?id={GSID}&graph={GRAPH}
//GlycanSampleState (gss)
export const glyconavisample = {
    URL: "https://glyconavi.org/GlycoAbun/Sample/index.php"
};

//https://test.glyconavi.org/GlycoAbun/Glycan/index.php?id={GSID}&graph={GRAPH}
//GlycanSample (gs)
export const glyconaviglycan = {
    URL: "https://glyconavi.org/GlycoAbun/Glycan/index.php"
};

//https://test.glyconavi.org/GlycoAbun/Glycan/index.php?id={DGID}
//Disease group
export const diseasegroupgraph = {
    URL: "https://glyconavi.org/GlycoAbun/Group/index.php"
};