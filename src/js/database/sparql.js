//@flow

"use strict";

//https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseSample?graph={GRAPH}
export const glycansample = {
    URL: "https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseSample"
};

export const diseasegroup = {
    URL: "https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseGroup"
};

//https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseGroup?id={DGID}
export const diseasegroupid = {
    URL: "https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseGroupID"
};

//https://sparqlist.glyconavi.org/api/GlycoNAVI_GlycanSample?graph={GRAPH}&id={GSID}
export const glycanabundance = {
    URL: "https://sparqlist.glyconavi.org/api/GlycoNAVI_GlycanSample"
};

//https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseSampleID?graph={GRAPH}&id={GSID}
export const glycansampleid = {
    URL: "https://sparqlist.glyconavi.org/api/GlycoNAVI_DiseaseSampleID"
};

//https://sparqlist.glyconavi.org/api/DOID?id={DOID}
//DOID %3A(:) 4
export const diseaseontologyidentifier = {
    URL: "https://sparqlist.glyconavi.org/api/DOID?id="
};