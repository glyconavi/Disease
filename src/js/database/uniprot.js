//@flow

"use strict";

//https://www.uniprot.org/uniprot/{UniprotID}
export const uniprot = {
    URL: "https://www.uniprot.org/uniprot/"
};