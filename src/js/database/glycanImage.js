//@flow

"use strict";

//https://image.glycosmos.org/snfg/png/{GlyTouCanID}
export const glycanImage = {
    URL: "https://image.glycosmos.org/snfg/png/"
};

//https://glyconavi.org/hub/img/?id={STR_3}
//https://glyconavi.org/hub/img/?id={GS_187_G2}
//https://glyconavi.org/hub/img/?id={G69411IG}
export const strImage = {
    URL: "https://glyconavi.org/hub/img/?id="
};

//https://glycan.sakura.ne.jp/public/str/glycan/str/img/{imageid}
/*
export const strImage = {
    URL: "https://glycan.sakura.ne.jp/public/str/glycan/str/img/"
};
 */