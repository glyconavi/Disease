//@flow

"use strict";

export const journalprofileparper = {
    title           : Symbol(),
    //Source  : Symbol(),
    authors         : Symbol(),
    volume          : Symbol(),
    issue           : Symbol(),
    pages           : Symbol(),
    pubdate         : Symbol(),
    issn            : Symbol(),
    essn            : Symbol(),
    fulljournalname : Symbol(),
    articleids      : Symbol(),
    uid             : Symbol(),

    author          : Symbol(),
};

export const journalprofilebook = {

};