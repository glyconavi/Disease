//@flow

"use strict";

const numberToMonth = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "Mar",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
};

const getNumbers = () => {
    return Object.keys(numberToMonth);
};

const getMonth = () => {
    return getNumbers(numberToMonth).map( (key) => {
        return numberToMonth[key];
    });
};

export const getNumberToMonth = (key) => {
    return numberToMonth[getNumbers(numberToMonth).filter( (k) => {
        return Number(key) === Number(k);
    }).pop() || ""];
};