//@flow

"use strict";

//Disease, Organism, Taxon, Sex, Solution, CellLine, Gene, Protein,

export const aggregatlist = {
    Disease         : Symbol(),
    Organism        : Symbol(),
    Taxon           : Symbol(),
    Sex             : Symbol(),
    Solution        : Symbol(),
    HostCellLine    : Symbol(),
    CellLine        : Symbol(),
    Gene            : Symbol(),
    Protein         : Symbol(),
};