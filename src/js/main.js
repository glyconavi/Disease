//@flow

"use strict";

import {showDiseaseGroup} from "./script/group/showDiseaseGroup";
import {showDiseaseSampleChart} from "./script/sample/makeStaticalGraph";
import {showDatabaseResourceis, showDiseaseGroupList} from "./script/abundance/makeAbundanceTable";
import {showGlycanAbundanceWithID, showGlycanSampleWithID} from "./script/abundance/showGlycanSample";
import {showDiseaseGroupWithID} from "./script/group/showDiseaseGroup";
import {showGlycanSampleEvent} from "./script/abundance/showGlycanSample";

window.showDiseaseGroup = showDiseaseGroup;
window.showGlycanSampleState = showDiseaseSampleChart;
window.showDiseaseAbundance = showDiseaseGroupList;

// for GlycoNAVI
window.showDiseaseGroupWithID = showDiseaseGroupWithID;
//window.showGlycanAbundanceWithID = showGlycanAbundanceWithID;
window.showGlycanSampleWithID = showGlycanSampleWithID;

//
window.showDatabaseResourceis = showDatabaseResourceis;
//window.showGlycanSampleEvent = showGlycanSampleEvent;

export let searchWord: Object = {};