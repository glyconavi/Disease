//@flow

"use strict";

import React from "react";
import {diseasegroupgraph, glyconaviglycan, glyconavisample} from "../database/glyconavi";
import {showGlycanAbundanceEventNewWindow, showGlycanSampleEventNewWindow} from "../script/abundance/showGlycanSample";
import Fab from "@material-ui/core/Fab";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";

export default function DiseaseDBResources (_props) {
    const samples: Object = _props.samples;
    const diseasegroup: Array<Object> = _props.group;
    let style: Object = {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
    };

    const handleClick = (e) => {
        const id: string = `DG_${e.currentTarget.innerText}`;
        window.open(`${diseasegroupgraph.URL}?id=${id}`);
    };

    return (
        <div class={"database_entries"}>
            <div style={{display: "flex", flexDirection: "row", alignItems: "center",}}>
                <div id={"legend_1"} style={{display: "flex", flexDirection: "row", alignItems: "center", marginRight: "4px",}}>
                    <Avatar
                        id={"disease_legend"}
                        alt=" "
                        src=" "
                        style={{
                            width: "15px",
                            height: "15px",
                            backgroundColor: "rgba(199,67,255,0.31)",
                            marginRight: "2px",
                            cursor: "pointer",
                        }}
                    />
                    <div>: disease</div>
                </div>

                <div id={"legend_2"} style={{display: "flex", flexDirection: "row", alignItems: "center", marginRight: "4px",}}>
                    <Avatar
                        id={"milk_legend"}
                        alt=" "
                        src=" "
                        style={{
                            width: "15px",
                            height: "15px",
                            backgroundColor: "#ffca71",
                            marginRight: "2px",
                            cursor: "pointer",
                        }}
                    />
                    <div>: milk oligosaccharide</div>
                </div>

                <div id={"legend_3"}  style={{display: "flex", flexDirection: "row", alignItems: "center", marginRight: "4px",}}>
                    <Avatar
                        id={"other_legend"}
                        alt=" "
                        src=" "
                        style={{
                            width: "15px",
                            height: "15px",
                            backgroundColor: "#8cff76",
                            marginRight: "2px",
                            cursor: "pointer",
                        }}
                    />
                    <div>: other samples</div>
                </div>
            </div>

            <div id={"content"} style={style}>
                {Object.keys(samples)
                    .sort(compare)
                    .map( (gsid) => {
                        return (
                            <div id={`resource_${gsid}`}
                                style={{
                                    border: "1px solid #ddd",
                                    margin: "2px 2px 2px 2px",
                                    backgroundColor: parseGraphColor(parseGraphType(samples[gsid].GraphType))
                                }}
                            >
                                <h4>{gsid}</h4>
                                <div id={"group"} style={{display: "flex", flexDirection: "row", alignItems: "center"}}>
                                    DG:
                                    {
                                        haveDiseaseGroup(diseasegroup, gsid).split(", ").map( (id) => {
                                            if (id === "") return;
                                            return (
                                                <Chip
                                                    style={{
                                                        marginLeft: "2px"
                                                    }}
                                                    label={id}
                                                    onClick={handleClick}
                                                />
                                            );
                                        })
                                    }
                                </div>
                                <Fab
                                    id={gsid}
                                    size="small"
                                    href={`${glyconavisample.URL}?id=${gsid}&graph=${parseGraphType(samples[gsid].GraphType)}`}
                                    variant="outlined"
                                    target={"_blank"}
                                    rel={"noopener"}
                                    style={{backgroundColor: "#1f9aaa"}}
                                >
                                S
                                </Fab>
                                <Fab
                                    id={gsid}
                                    size="small"
                                    variant="outlined"
                                    target={"_blank"}
                                    rel={"noopener"}
                                    onClick={(e) => showGlycanSampleEventNewWindow(e)}
                                    color={"primary"}
                                    style={{backgroundColor: "#aa695a"}}
                                    value={parseGraphType(samples[gsid].GraphType)}
                                >
                                OS
                                </Fab>
                                <Fab
                                    id={gsid}
                                    size="small"
                                    variant="outlined"
                                    target={"_blank"}
                                    rel={"noopener"}
                                    onClick={(e) => showGlycanAbundanceEventNewWindow(e)}
                                    color={"primary"}
                                    style={{backgroundColor: "#9f51aa"}}
                                    value={parseGraphType(samples[gsid].GraphType)}
                                >
                                OA
                                </Fab>
                            </div>
                        );
                    })}
            </div>
        </div>
    );
}

const haveDiseaseGroup = (_diseasegroup: Array<Object>, _gsid: string): boolean => {
    let ret: string = "";
    _diseasegroup.map( (content) => {
        if (_gsid === content.GlycanSampleID) {
            if (ret === "") {
                ret = content.DiseaseGroupID.replace("DG_", "");
            } else {
                ret = `${ret}, ${content.DiseaseGroupID.replace("DG_", "")}`;
            }
        }
    });

    return ret;
};

const parseGraphColor = (_graph: string): string => {
    if (_graph === null) return "#FFFFFF";
    if (_graph === "disease") return "rgba(199,67,255,0.31)";
    if (_graph === "milksugar") return "#ffca71";
    if (_graph === "taxonomy") return "#FFFFFF";
    if (_graph === "other") return "#8cff76";
    return "#FFFFFF";
};

const parseGraphType = (_graph: string): string => {
    const ret: Array<string> = _graph.match(/(disease|milksugar|other)/);
    if (ret === null) return "";
    return ret[0];
};

const compare = (a, b) => {
    let q1: number = Number(a.replace("GS_", ""));
    let q2: number = Number(b.replace("GS_", ""));
    return (q1 < q2 ? -1 : 1);
};

const makebgColor = () => {
    const min: number = 1;
    const max: number = 255;

    const r: number = Math.floor( Math.random() * (max + 1 - min) ) + min ;
    const g: number = Math.floor( Math.random() * (max + 1 - min) ) + min ;
    const b: number = Math.floor( Math.random() * (max + 1 - min) ) + min ;
    return `rgba(${r}, ${g}, ${b}, .3)`;
};