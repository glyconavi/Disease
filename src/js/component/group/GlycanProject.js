//@flow

"use strict";

import React from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {makeStyles, Paper} from "@material-ui/core";
import PropTypes from "prop-types";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function GlycanProject(_props) {
    const classes = useStyles();
    const glycanProject: Object = _props.glycanproject;
    const subGroup: Object = _props.subgroup;

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableBody>
                    {Object.keys(glycanProject).sort().map((gpID) => (
                        <TableRow key={gpID}>
                            <TableCell>{gpID}</TableCell>
                            <TableCell>{glycanProject[gpID].Title}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

GlycanProject.propTypes = {
    glycanproject: PropTypes.object.isRequired,
    subgroup: PropTypes.object.isRequired
};