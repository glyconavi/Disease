//@flow

"use strict";

import * as React from "react";
import ImageBlock from "./ImageBlock";

export default class GlycanImageList extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};
    }

    render () {
        const subset: Object = this.props.subset;
        const subkey: string = this.props.subkey;
        const gtcid: Object = this.props.gtcid;

        let imageids: Array<string> = [];
        subset[subkey].label.map( (key) => {
            let id: string = gtcid[key][0].replace("Glycan:", "");

            let ids: Array<string> = id.match(/G[0-9]{5}[A-Z]{2}/g);
            if (ids === null) {
                imageids.push(id.replace("&", "-"));
                /*
                id.split("&").map( (id) => {
                    imageids.push(id);
                });
                 */
            } else {
                imageids = imageids.concat(ids);
            }
        });

        return (
            <ImageBlock imageids={imageids} />
        );
    }
}