//@flow

"use strict";

import {strImage} from "../../database/glycanImage";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {glytoucan} from "../../database/glycanrepository";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        marginRight: "10px",
        marginLeft: "10px",
        marginBottom: "10px"
    },
    media: {
        height: 140,
    },
});

export default function ImageBlock (_props) {
    const classes = useStyles();

    return (
        _props.imageids.map((id) => {
            return (
                <div
                    style={{
                        marginRight: "10px",
                        marginLeft: "10px"
                    }}>
                    <h5>{id}</h5>
                    <img src={`${strImage.URL}${id}`} alt={"Missing image"}/>
                </div>
            );
        })
    );
    /*
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h4">
                                {id}
                            </Typography>
                        </CardContent>
                        <CardMedia
                            className={classes.media}
                            image={`${strImage.URL}${id}`}
                            title={`${id}`}
                        />
                    </CardActionArea>
                    <CardActions>
                        <Button size="small" color="primary">
                            GlyCosmos
                        </Button>
                        <Button size="small" color="primary" href={`${glytoucan.URL}${id}`} target={"_blank"}>
                            GlyTouCan
                        </Button>
                    </CardActions>
                </Card>
                 */
}