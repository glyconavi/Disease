//@flow

"use strict";

import TableContainer from "@material-ui/core/TableContainer";
import {makeStyles, useTheme, withStyles} from "@material-ui/core/styles";
import {Paper} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import React from "react";
import IconButton from "@material-ui/core/IconButton";
import PropTypes from "prop-types";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import {AbundanceTableHead} from "../abundance/AbundanceTableHead";
import TableHead from "@material-ui/core/TableHead";
import TextComponent from "../common/TextComponent";
import {glyconaviglycan, glyconavisample} from "../../database/glyconavi";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    paper: {
        width: "100%",
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 650,
    },
    visuallyHidden: {
        border: 0,
        clip: "rect(0 0 0 0)",
        height: 1,
        margin: -1,
        overflow: "hidden",
        padding: 0,
        position: "absolute",
        top: 20,
        width: 1,
    },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function TablePaginationActions(props) {
    const classes = useStyles();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;

    const handleFirstPageButtonClick = (event) => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <div className={classes.root}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                {theme.direction === "rtl" ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === "rtl" ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </div>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

export default function GlycanSample(_props) {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [order, setOrder] = React.useState("asc");
    const [orderBy, setOrderBy] = React.useState();
    const [selected, setSelected] = React.useState([]);

    const rows: Array<Object> = _props.rows;
    const columns: Array<string> = _props.columns;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(property);
    };

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead style={{display: "none"}}>
                    <AbundanceTableHead
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                        columns={columns}
                        style={{display: "none"}}
                    />
                </TableHead>
                <TableBody>
                    {rows.map( (row) => {
                        return (
                            <StyledTableRow key={row.GlycanSampleID}>
                                <StyledTableCell>{row.GlycanSampleID}</StyledTableCell>
                                <StyledTableCell><TextComponent subject={""} text={row.Description} /></StyledTableCell>
                                <StyledTableCell>
                                    <Avatar
                                        id={`${row.GlycanSampleID}_dgs`}
                                        alt="S"
                                        src=" "
                                        style={{
                                            backgroundColor: "#5b8eb7",
                                            cursor: "pointer",
                                            marginLeft: "2px",
                                            marginRight: "2px",
                                        }}
                                        target={"_blank"}
                                        rel={"noopener"}
                                        onClick={() => {window.open(`${glyconavisample.URL}?id=${row.GlycanSampleID}&graph=disease`);}}
                                        onMouseOver={ (e) => {e.target.style.backgroundColor = "#666";} }
                                        onMouseOut={ (e) => {e.target.style.backgroundColor = "#5b8eb7";} }
                                    />
                                </StyledTableCell>
                            </StyledTableRow>
                        );
                    })}
                    <StyledTableRow key={"keylegend"}>
                        <StyledTableCell colSpan={4} >S: Glycan Sample</StyledTableCell>
                    </StyledTableRow>
                </TableBody>
            </Table>
            <TableFooter style={{display: "none"}}>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25, 50, 100, {label: "All", value: rows.length}]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                        inputProps: {"aria-label": "rows per page"},
                        native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                />
            </TableFooter>
        </TableContainer>
    );
}

GlycanSample.propTypes = {
    rows: PropTypes.object.isRequired,
    columns: PropTypes.object.isRequired
};

/*
                                <StyledTableCell>
                                    <Avatar
                                        id={`${row.GlycanSampleID}_dgg`}
                                        alt="G"
                                        src=" "
                                        style={{
                                            backgroundColor: "#ef857d",
                                            cursor: "pointer",
                                            marginLeft: "2px",
                                            marginRight: "2px",
                                        }}
                                        target={"_blank"}
                                        rel={"noopener"}
                                        onClick={() => {window.open(`${glyconaviglycan.URL}?id=${row.GlycanSampleID}&graph=disease`);}}
                                        onMouseOver={ (e) => {e.target.style.backgroundColor = "#666";} }
                                        onMouseOut={ (e) => {e.target.style.backgroundColor = "#ef857d";} }
                                    />
                                </StyledTableCell>

 */