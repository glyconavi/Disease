//@flow

"use strict";

import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import GlycanImageList from "./GlycanImageList";
import RelativeAbundanceTable from "./RelativeAbundanceTable";

const useStyles = makeStyles((theme) => ({
    root: {
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));

const StyledAccordionDetails = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(AccordionDetails);

export default function GroupUtilities (_props) {
    const classes = useStyles();

    const imgstyle: Object = {
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    };
    //ret.style.border = "solid #ddd 1px";
    //ret.style.width = "80%";

    // make abundance data
    let columns: Array<string> = [""];
    let rows: Array<Object> = [];

    // make columns
    _props.subset[_props.subkey].label.map((label) => {
        const relativelabel: string = _props.gtcid[label][0].replace("Glycan:", "");
        const gtcID: Array<string> = relativelabel.match(/G[0-9]{5}[A-Z]{2}/g);
        if (gtcID !== null) {
            columns.push(gtcID.join(", "));
        } else {
            columns.push(relativelabel);
        }
    });

    // make rows
    Object.keys(_props.subset[_props.subkey]).map( (content) => {
        if (content === "label") return;
        let item: Array<string> = [];
        item.push(_props.description[content]);
        _props.subset[_props.subkey][content].params.map( (abundance) => {
            item.push(abundance);
        });
        rows.push(item);
    });

    return (
        <div className={classes.root}>
            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="glycan-content"
                    id="glycan-header"
                >
                    <Typography className={classes.heading}>{`Glycan images: ${_props.subkey}`}</Typography>
                </AccordionSummary>
                <StyledAccordionDetails>
                    <Typography>
                        <div id={`${_props.subkey}_images`} style={imgstyle} >
                            <GlycanImageList subset={_props.subset} subkey={_props.subkey} gtcid={_props.gtcid} />
                        </div>
                    </Typography>
                </StyledAccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="relativeabundance-content"
                    id="relativeabundance-header"
                >
                    <Typography className={classes.heading}>{`Relative abundance: ${_props.subkey}`}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <div id={`${_props.subkey}_abundance`}>
                            <RelativeAbundanceTable columns={columns} rows={rows} />
                        </div>
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </div>
    );
}