//@flow

"use strict";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import React from "react";
import PropTypes from "prop-types";

export function AbundanceTableHead (props) {
    const { order, orderBy, onRequestSort, columns } = props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableRow>
            {columns.map((headCell) => (
                <TableCell
                    key={headCell}
                    padding={headCell.disablePadding ? "none" : "default"}
                    sortDirection={orderBy === headCell ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === headCell}
                        direction={orderBy === headCell ? order : "asc"}
                        onClick={createSortHandler(headCell)}
                    >
                        {headCell}
                    </TableSortLabel>
                </TableCell>
            ))}
        </TableRow>
    );
}

AbundanceTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(["asc", "desc"]).isRequired,
    orderBy: PropTypes.string.isRequired,
    columns: PropTypes.object.isRequired
};