//@flow

"use strict";

import React from "react";
import ReactDOM from "react-dom";
import AbundanceTable from "./AbundanceTable";

//Reference: https://stackoverflow.com/questions/47574490/open-a-component-in-new-window-on-a-click-in-react
//Reference: https://python5.com/q/xyiklxgw
//Reference: https://stackoverflow.com/questions/63447174/execute-jquery-in-react-portals
export default class OpenWindowComponent extends React.Component {
    constructor(props) {
        super(props);
        this.containerEl = document.createElement("div");
        this.externalWindow = null;
    }

    render() {
        if (!this.containerEl) {
            return null;
        }
        return ReactDOM.createPortal(this.props.children, this.containerEl);
    }

    copyStyles(sourceDoc, targetDoc) {
        Array.from(sourceDoc.styleSheets).forEach(styleSheet => {
            if (styleSheet.cssRules) { // for <style> elements
                const newStyleEl = sourceDoc.createElement("style");

                Array.from(styleSheet.cssRules).forEach(cssRule => {
                    // write the text of each rule into the body of the style element
                    newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText));
                });

                targetDoc.head.appendChild(newStyleEl);
            } else if (styleSheet.href) { // for <link> elements loading CSS from a URL
                const newLinkEl = sourceDoc.createElement("link");

                newLinkEl.rel = "stylesheet";
                newLinkEl.href = styleSheet.href;
                targetDoc.head.appendChild(newLinkEl);
            }
        });
    }

    componentDidMount() {
        this.externalWindow = window.open("", "", "fullscreen=1,scrollbars=1,toolbar=1,menubar=1,staus=1,resizable=1");
        this.containerEl.id = this.props.gsid;

        let header: HTMLElement = document.createElement("h4");
        header.innerText = "Glyco Form";
        this.externalWindow.document.body.appendChild(header);

        this.externalWindow.document.body.appendChild(this.containerEl);

        ReactDOM.render(
            <AbundanceTable columns={this.props.columns} rows={this.props.rows} throw={this.props.throw} />,
            this.containerEl
        );

        // copy style
        this.copyStyles(document, this.externalWindow.document);

        let title: HTMLElement = document.createElement("title");
        title.innerText = `GlycoNAVI - DiseaseAbundance ${this.props.gsid}`;
        this.externalWindow.document.head.appendChild(title);
    }

    componentWillUnmount() {
        this.externalWindow.close();
    }
}