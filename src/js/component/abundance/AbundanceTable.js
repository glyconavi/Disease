//@flow

"use strict";

import React from "react";
import TextComponent from "../common/TextComponent";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {makeStyles, useTheme, withStyles} from "@material-ui/core/styles";
import {Paper} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import PropTypes from "prop-types";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import {AbundanceTableHead} from "./AbundanceTableHead";
import TableHead from "@material-ui/core/TableHead";
import Search from "../common/Search";
import {setKeyWordFromSearchForm, sortAbundanceList} from "../../script/abundance/makeAbundanceTable";
import GlycanImage from "../common/GlycanImage";
import DetailAvatar from "../common/DetailAvatar";
import GTCIDAvatar from "../common/GTCIDAvatar";
import {glyconavisample} from "../../database/glyconavi";

const useStyles = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    paper: {
        width: "100%",
        marginBottom: theme.spacing(2),
    },
    visuallyHidden: {
        border: 0,
        clip: "rect(0 0 0 0)",
        height: 1,
        margin: -1,
        overflow: "hidden",
        padding: 0,
        position: "absolute",
        top: 20,
        width: 1,
    },
    popper: {
        border: "1px solid",
        padding: theme.spacing(1),
        backgroundColor: theme.palette.background.popper,
    }
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function TablePaginationActions(props) {
    const classes = useStyles();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;

    const handleFirstPageButtonClick = (event) => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <div className={classes.root}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                {theme.direction === "rtl" ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === "rtl" ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </div>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

export default function AbundanceTable (_props: Object) {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [order, setOrder] = React.useState("asc");
    const [orderBy, setOrderBy] = React.useState();
    const [selected, setSelected] = React.useState([]);
    const [anchorEl] = React.useState(null);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(property);
    };

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const columns: Array<string> = _props.columns;
    const rows: Array<Object> = _props.rows;
    const throwList: Array<string> = _props.throw;

    const open = Boolean(anchorEl);
    const id = open ? "simple-popper" : undefined;

    return (
        <TableContainer component={Paper}>
            <Search search={ (keyword) => {setKeyWordFromSearchForm(keyword); }} sortable={ () => {sortAbundanceList(rows); }} />
            <Table size="small" aria-label="a dense table">
                <TableHead>
                    <AbundanceTableHead
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                        columns={columns}
                    />
                </TableHead>
                <TableBody>
                    {
                        stableSort(rows, getComparator(order, orderBy))
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((rows, index) => {
                                return (
                                    <StyledTableRow
                                        key={index}
                                        hover
                                        onClick={(event) => handleClick(event, rows.name)}
                                        role="checkbox"
                                        tabIndex={-1}
                                    >
                                        {Object.keys(rows).map((key) => {
                                            if (throwList.indexOf(key) !== -1) return;
                                            if (key === "GlycanSampleID") {
                                                return <StyledTableCell><a id={rows[key]} target={"_blank"} href={`${glyconavisample.URL}?id=${rows[key]}&graph=${parseGraphType(rows.GraphType)}`}>{rows[key]}</a></StyledTableCell>;
                                                //return <StyledTableCell><DetailAvatar id={rows[key]} avatar1={"das"} avatar2={"dag"} graphtype={rows.GraphType} /></StyledTableCell>;
                                            } else if (key === "Image") {
                                                return <StyledTableCell><GlycanImage id={rows[key]} /></StyledTableCell>;
                                            } else if (key === "GlyTouCan_ID") {
                                                return <StyledTableCell><GTCIDAvatar id={rows[key]} /></StyledTableCell>;
                                            } else {
                                                return <StyledTableCell><TextComponent subject={key.replace("_", " ")} text={rows[key]} /></StyledTableCell>;
                                            }
                                        })}
                                    </StyledTableRow>
                                );
                            })
                    }
                </TableBody>
            </Table>
            <TableFooter>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25, 50, 100, {label: "All", value: rows.length}]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                        inputProps: {"aria-label": "rows per page"},
                        native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                />
            </TableFooter>
        </TableContainer>
    );
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

function descendingComparator(a, b, orderBy) {
    if (orderBy !== undefined) orderBy = orderBy.replace(" ", "_");
    const q1 = a[orderBy];
    const q2 = b[orderBy];
    if (q2 < q1) {
        return -1;
    }
    if (q2 > q1) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === "desc"
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

AbundanceTable.propTypes = {
    rows: PropTypes.object.isRequired,
    columns: PropTypes.object.isRequired,
    throw: PropTypes.object.isRequired,
};

const parseGraphType = (_graph: string): string => {
    const ret: Array<string> = _graph.match(/(disease|milksugar|other)/);
    if (ret === null) return "";
    return ret[0];
};

/*
                                                        <Fab
                                                            id={rows[key]}
                                                            size="small"
                                                            href={`${glyconavisample.URL}${rows[key]}`}
                                                            variant="outlined"
                                                            target={"_blank"}
                                                            rel={"noopener"}
                                                            style={{
                                                                "background": "#efefef",
                                                                "color": "#0068b7"
                                                            }}
                                                        >
                                                            S
                                                        </Fab>
 <Fab
                                                            id={rows[key]}
                                                            size="small"
                                                            href={`${glyconaviglycan.URL}${rows[key]}`}
                                                            variant="outlined"
                                                            target={"_blank"}
                                                            rel={"noopener"}
                                                            style={{
                                                                "background": "#efefef",
                                                                "color": "#ef857d"
                                                            }}
                                                        >
                                                            G
                                                        </Fab>
 */