//@flow

"use strict";

import React from "react";
import Chip from "@material-ui/core/Chip";
import {keyTips, makeStaticalGraph} from "../../script/sample/makeStaticalGraph";

export default class KeyWordChips extends React.Component {
    constructor (props) {
        super(props);
        this.state = this.props.chips;
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete (event) {
        let parentNode: HTMLElement = event.currentTarget.parentNode;
        let id: string = parentNode.id;
        //let content: string = parentNode.textContent;
        let state: Object = this.state;
        delete state[id];
        this.setState(state);

        this.props.delete(id);
        makeStaticalGraph(this.props.sample);
        this.props.sorttable(this.props.sample);
    }

    componentDidUpdate () {
    }

    render () {
        let chipLists = [];
        for (let key: string of Object.keys(keyTips)) {
            chipLists.push(
                <Chip size="small"
                    id={key}
                    label={keyTips[key]}
                    onDelete={(event) => this.handleDelete(event)}
                    color="primary"/>
            );
        }
        return (
            <div>
                <h2>Keyword</h2>
                {chipLists}
            </div>
        );
    }
}