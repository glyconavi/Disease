//@flow

"use strict";

import ReactDOM from "react-dom";
import React from "react";
import GlycanSampleProfile from "./GlycanSampleProfile";

export default class OpenWindowSampleProfile extends React.Component {
    constructor (props) {
        super (props);
        this.state = {};
        this.containerEl = document.createElement("div");
        this.externalWindow = null;
    }

    render() {
        if (!this.containerEl) {
            return null;
        }
        return ReactDOM.createPortal(this.props.children, this.containerEl);
    }

    copyStyles(sourceDoc, targetDoc) {
        Array.from(sourceDoc.styleSheets).forEach(styleSheet => {
            if (styleSheet.cssRules) { // for <style> elements
                const newStyleEl = sourceDoc.createElement("style");

                Array.from(styleSheet.cssRules).forEach(cssRule => {
                    // write the text of each rule into the body of the style element
                    newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText));
                });

                targetDoc.head.appendChild(newStyleEl);
            } else if (styleSheet.href) { // for <link> elements loading CSS from a URL
                const newLinkEl = sourceDoc.createElement("link");

                newLinkEl.rel = "stylesheet";
                newLinkEl.href = styleSheet.href;
                targetDoc.head.appendChild(newLinkEl);
            }
        });
    }

    componentDidMount() {
        this.externalWindow = window.open("", "", "fullscreen=1,scrollbars=1,toolbar=1,menubar=1,staus=1,resizable=1");
        this.containerEl.id = this.props.gsid;

        this.externalWindow.document.body.appendChild(this.containerEl);

        ReactDOM.render(
            <GlycanSampleProfile rows={this.props.rows} gsid={this.props.gsid} journal={this.props.journal} />,
            this.containerEl
        );

        // copy style
        this.copyStyles(document, this.externalWindow.document);

        let title: HTMLElement = document.createElement("title");
        title.innerText = `GlycoNAVI - GlycanSample Profile ${this.props.gsid}`;
        this.externalWindow.document.head.appendChild(title);
    }

    componentWillUnmount() {
        this.externalWindow.close();
    }
}