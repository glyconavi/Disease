//@flow

"use strict";

import React from "react";
import {makeStyles, withStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import TextComponent from "../common/TextComponent";
import JournalProfile from "../common/JournalProfile";
import PropTypes from "prop-types";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

export default function GlycanSampleProfile (_props: Object) {
    const classes = useStyles();
    const rows: Array<Object> = _props.rows;
    const gsid: string = _props.gsid;
    const journal: Object = _props.journal;

    return (
        <div id={gsid + "_profile"}>
            <h4> Glycan Sample Profile </h4>
            <div id={gsid + "_sample"} style={{width: "100%"}} >
                <TableContainer component={Paper}>
                    <Table className={classes.table} size="small" aria-label="simple table">
                        <TableBody>
                            {rows.map( (row, index) => {
                                if (row.key === "GraphType") return;
                                return (
                                    <StyledTableRow key={gsid + "_" + index}>
                                        <StyledTableCell style={{ fontWeight: "bold" }} >{row.key}</StyledTableCell>
                                        <StyledTableCell><TextComponent subject={row.key} text={row.value} /></StyledTableCell>
                                    </StyledTableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>

            <h4> References </h4>
            <div id={gsid + "_journal"} style={{width: "100%"}} >
                <JournalProfile journal={journal} />
            </div>
        </div>
    );
}

GlycanSampleProfile.propTypes = {
    rows: PropTypes.object.isRequired,
    journal: PropTypes.object.isRequired
};