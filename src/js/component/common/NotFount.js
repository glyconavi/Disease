//@flow

"use strict";

import React from "react";

export default class NotFount extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return(
            <div>Not Found</div>
        );
    }
}