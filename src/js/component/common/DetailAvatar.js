//@flow

"use strict";

import Avatar from "@material-ui/core/Avatar";
import {glyconaviglycan, glyconavisample} from "../../database/glyconavi";
import React from "react";

export default function DetailAvatar (_props) {
    const id: string = _props.id;
    const style: Object = {
        display:"flex",
        flexDirection: "row",
        alignItems: "center"
    };

    return (
        <div style={style} >
            {id}
            <Avatar
                id={`${id}_${_props.avatar1}`}
                alt="S"
                src=" "
                style={{
                    backgroundColor: "#5b8eb7",
                    cursor: "pointer",
                    marginLeft: "2px",
                    marginRight: "2px",
                }}
                target={"_blank"}
                rel={"noopener"}
                onClick={() => {window.open(`${glyconavisample.URL}?id=${id}&graph=${parseGraphType(_props.graphtype)}`);}}
                onMouseOver={ (e) => {e.target.style.backgroundColor = "#666";} }
                onMouseOut={ (e) => {e.target.style.backgroundColor = "#5b8eb7";} }
            />
        </div>
    );
}

const parseGraphType = (_graph: string): string => {
    const ret: Array<string> = _graph.match(/(disease|milksugar)/);
    if (ret === null) return "";
    return ret[0];
};

/*
            <Avatar
                id={`${id}_${_props.avatar2}`}
                alt="G"
                src=" "
                style={{
                    backgroundColor: "#ef857d",
                    cursor: "pointer",
                    marginLeft: "2px",
                    marginRight: "2px",
                }}
                target={"_blank"}
                rel={"noopener"}
                onClick={() => {window.open(`${glyconaviglycan.URL}?id=${id}&graph=${parseGraphType(_props.graphtype)}`);}}
                onMouseOver={ (e) => {e.target.style.backgroundColor = "#666";} }
                onMouseOut={ (e) => {e.target.style.backgroundColor = "#ef857d";} }
            />
 */