//@flow

"use strict";

import React from "react";
import Avatar from "@material-ui/core/Avatar";
import {glytoucan} from "../../database/glycanrepository";
import glytoucanlogo from "../../../resources/image/glytoucan_logo.png";
import glycosmoslogo from "../../../resources/image/GlyCosmos_Logo.png";
//../../img/

export default function GTCIDAvatar (_props) {
    const gtcID: Array<string> = _props.id.match(/G[0-9]{5}[A-Z]{2}/g);
    if (gtcID === null) return (<div/>);
    const style: Object = {
        display:"flex",
        flexDirection: "row",
        alignItems: "center",
    };

    return (
        gtcID.map( (gtcID) => {
            return (
                <div style={style}>
                    {gtcID}
                    <Avatar
                        id={"glytoucan"}
                        alt=" "
                        style={{
                            backgroundColor: "#0068b7",
                            cursor: "pointer",
                            marginLeft: "2px",
                            marginRight: "2px",
                        }}
                        target={"_blank"}
                        rel={"noopener"}
                        onClick={() => {window.open(`${glytoucan.URL}${gtcID}`);}}
                        src={glytoucanlogo} />
                    <Avatar
                        id={"glycosmos"}
                        alt=" "
                        style={{
                            backgroundColor: "#7e5abf",
                            cursor: "pointer",
                            marginLeft: "2px",
                            marginRight: "2px",
                        }}
                        target={"_blank"}
                        rel={"noopener"}
                        onClick={() => {}}
                        src={glycosmoslogo} />
                </div>
            );
        })
    );
}