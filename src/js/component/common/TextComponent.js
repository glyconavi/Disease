//@flow

"use strict";

import React from "react";
import {mesh} from "../../database/medicalSubjectHeadings";
import {uniprot} from "../../database/uniprot";
import {doi, pubmed} from "../../database/pubmed";
import {diseaseOntology} from "../../database/diseaseOntology";
import {glyconaviglycan, diseasegroupgraph, glyconavisample} from "../../database/glyconavi";

export default class TextComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    makeHTMLElement (_subject: string, _content: string) {
        switch (_subject) {
            case "MESH":
                return <a href={mesh.URL + _content} target={"_blank"} rel="noopener noreferrer">{_content}</a>;
            case "UniprotID": {
                let ret: Array<string> = [];

                //https://registry.identifiers.org/registry/uniprot
                const regex: string = /^([A-N,R-Z][0-9]([A-Z][A-Z, 0-9][A-Z, 0-9][0-9]){1,2})|([O,P,Q][0-9][A-Z, 0-9][A-Z, 0-9][A-Z, 0-9][0-9])(\.\d+)?$/g;
                let ids: Array<string> = _content.match(regex);
                if (ids === null) return ret;
                ids.map( (id) => {
                    ret.push(<a href={uniprot.URL + id} target={"_blank"} rel="noopener noreferrer">{id}</a>);
                    ret.push(" ");
                });

                return ret;
            }
            case "Reference":
                return <a href={pubmed.URL + _content} target="_blank" rel="noopener noreferrer">{_content}</a>;
            case "DOID":
                return <a href={diseaseOntology.URL + _content + "/"} target="_blank" rel="noopener noreferrer">{_content}</a>;
            case "PMID": {
                const pmid: string = _content.replace("PMID", "");
                return <a href={pubmed.URL + pmid} target="_blank" rel="noopener noreferrer">{pmid}</a>;
            }
            case "GlycanSampleID":
                return <a id={`glycansample_${_content}`} target={"_blank"} href={`${glyconavisample.URL}?id=${_content}&graph=disease`}>{_content}</a>;
            case "DiseaseGroupID":
                return <a id={_content} target={"_blank"} href={`${diseasegroupgraph.URL}?id=${_content}`} >{_content}</a>;
            case "doi":
                return <a id={"doi"} target={"_blank"} href={`${doi.URL}${_content}`}>{_content}</a>;
            default :
                return <div dangerouslySetInnerHTML={{__html: _content}} />;
        }
    }

    render () {
        return (
            <div>
                {this.makeHTMLElement(this.props.subject, this.props.text, this.props.graph)}
            </div>
        );
    }
}