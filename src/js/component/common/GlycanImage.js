//@flow

"use strict";

import * as React from "react";
import {strImage} from "../../database/glycanImage";

export default class GlycanImage extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};
    }

    render () {
        let imageID: string = this.props.id;

        imageID = imageID.replace("Glycan:", "");
        const gtcID: Array<string> = imageID.match(/G[0-9]{5}[A-Z]{2}/g);
        let ids: Array<string> = [];
        let style: Object = {};

        if (gtcID === null) {
            ids.push(imageID.replace("&", "-"));//imageID.split("&");
            style = {
                width: "20%",
                height: "auto"
            };
        } else {
            ids = ids.concat(gtcID);
            style = {
                width: "20%",
                height: "auto"
            };
        }

        return (
            ids.map( (id) => {
                return (
                    <img
                        alt={"Missing image"}
                        id={id}
                        src={`${strImage.URL}${id}`}
                        style={style}
                        onMouseOver={
                            (e) => {e.target.style.transform = "scale(2.0, 2.0)";}
                        }
                        onMouseOut={
                            (e) => {e.target.style.transform = "scale(1.0, 1.0)";}
                        }
                    />
                );
            })
        );
    }
}