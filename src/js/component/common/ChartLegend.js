//@flow

"use strict";

import React from "react";
import Avatar from "@material-ui/core/Avatar";
import {filterDiseaseSampleListWithEvent} from "../../script/sample/makeStaticalGraph";

export default function ChartLegend (_props) {
    const options: Object = _props.chartopt;
    const legend: Object = _props.legend;
    const titleClass: string = options.title.text;

    return (
        legend.legendItems.map( (legendItem) => {
            return (
                <div
                    class={`${titleClass}`}
                    id={`${legendItem.text}`}
                    style={{
                        cursor: "pointer",
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: "4px",
                        alignItems: "center",
                        fontSize: "12px",
                        fontStyle: "Helvetica Neue",
                    }}
                    onMouseOver={ (e) => { return _props.over(e); }}
                    onMouseOut={ (e) => { return (_props.out(legendItem, e)); }}
                    onClick={ (e) => { return _props.click(_props.samples, e); }}
                >
                    <Avatar
                        id={"legendcolor"}
                        alt=" "
                        src=" "
                        style={{
                            width: "15px",
                            height: "15px",
                            backgroundColor: `${legendItem.fillStyle}`,
                            marginRight: "2px",
                            cursor: "pointer",
                        }}
                    />
                    <div id={"samplename"} dangerouslySetInnerHTML={{__html: legendItem.text}} />
                </div>
            );
        })
    );
}

//on mouse over : backgroundColor: "#666";
//on mouse out : backgroundColor: "#fff"