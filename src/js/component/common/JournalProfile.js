//@flow

"use strict";

import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {journalprofileparper} from "../../parameters/journalprofile";
import PropTypes from "prop-types";
import TextComponent from "./TextComponent";

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    table: {
        minWidth: 350,
    },
});

export default function JournalProfile (_props: Object) {
    const classes = useStyles();

    const journal: Object = _props.journal;

    let items: Array<Object> = [];
    Object.keys(journal).forEach( (key) => {
        if (journalprofileparper[key] === undefined) return;

        let item: Object = parseJournalDetails(key, journal[key]);
        items.push(item);
    });

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableBody>
                    {items.map( (item, index) => {
                        return (
                            <StyledTableRow key={`journal_${index}`}>
                                <StyledTableCell component="th" scope="row" style={{ fontWeight: "bold" }}>
                                    {item.key}
                                </StyledTableCell>
                                <StyledTableCell align="left"><TextComponent subject={item.key} text={item.value}/></StyledTableCell>
                            </StyledTableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

const parseJournalDetails = (_key: string, _content: Object): Object => {
    const upperKey: string = _key.charAt(0).toUpperCase() + _key.slice(1);

    let ret: Object = {
        key: "",
        value: ""
    };

    switch (_key) {
        case "uid": {
            ret.key = "PMID";
            ret.value = _content;
            break;
        }
        case "articleids" : {
            _content.map( (id, index) => {
                if (id.idtype === "doi") {
                    ret.key = "doi";
                    ret.value = id.value;
                }
            });
            if (ret.key === "") {
                ret.key = "doi";
                ret.value = "";
            }

            break;
        }
        case "authors" : {
            ret.key = upperKey;
            _content.map( (author, index) => {
                let name: string = author.name;
                if (index !== 0) {
                    ret.value = `${ret.value}, ${name}`;
                } else {
                    ret.value = `${name}`;
                }
            });
            break;
        }
        case "author": {
            ret.key = upperKey + "s";
            _content.map( (author, index) => {
                let name: string = `${author.family} ${author.given}`;
                if (index !== 0) {
                    ret.value = `${ret.value}, ${name}`;
                } else {
                    ret.value = `${name}`;
                }
            });
            break;
        }
        default: {
            ret.key = upperKey;
            ret.value = _content;
        }
    }

    return ret;
};

JournalProfile.propTypes = {
    journal: PropTypes.object.isRequired
};