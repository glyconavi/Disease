//@flow

"use strict";

import {
    diseasegroup, diseasegroupid, glycanabundance, glycansample, glycansampleid,
} from "../../database/sparql";
import {crossrefapi, pubmedapi} from "../../database/pubmed";
import {diseaseOntology} from "../../database/diseaseOntology";
import {getNumberToMonth} from "../../parameters/month";

export const runDGIDSPARQL = (_dgid) => {
    return new Promise ((resolve, reject) => {
        setTimeout( function () {
            let apiURL = `${diseasegroupid.URL}?`;
            apiURL += `id=${_dgid}`;

            fetch(apiURL)
                .then(res => {
                    if (res.ok) {
                        return res.json();
                    } else {
                        alert("Request failed: " + res.status);
                    }
                })
                .then(json => {
                    let query = json[0];
                    resolve(query);
                })
                .catch(err => {
                    makeError(err);
                });
        },500);
    });
};

export const runDSSPARQL = (_graph, value): void => {
    return new Promise ((resolve, reject) => {
        setTimeout (function () {
            let apiURL = `${glycansample.URL}?`;
            apiURL += `graph=${encodeURI(_graph)}`;

            fetch (apiURL)
                .then (res => {
                    if (res.ok) {
                        return res.json();
                    } else {
                        alert("Request failed: " + res.status);
                    }
                })
                .then (json => {
                    let ret: Array<Object> = [];
                    ret = ret.concat(value);
                    ret.push(json[0]);
                    resolve(ret);
                })
                .catch (err => alert(err));
        }, 500);
    });
};

export const runDGSPARQL = (value): void => {
    return new Promise ((resolve, reject) => {
        setTimeout (function () {
            let apiURL: string = diseasegroup.URL;

            fetch (apiURL)
                .then (res => {
                    if (res.ok) {
                        return res.json();
                    } else {
                        alert("Request failed: " + res.status);
                    }
                })
                .then (json => {
                    resolve(json[0]);
                })
                .catch (err => alert(err));
        }, 500);
    });
};


export const runGSIDSPARQL = (_gsid: string, _graph: string) => {
    return new Promise ((resolve, reject) => {
        setTimeout (function () {
            let apiURL: string = `${glycansampleid.URL}?`;
            apiURL += `graph=${encodeURI(`http://glyconavi.org/${_graph}/`)}`;
            apiURL += `&id=${_gsid}`;

            fetch (apiURL).then (res => {
                if (res.ok) {
                    return res.json();
                } else {
                    alert("Request failed: " + res.status);
                }
            }).then (json => {
                resolve(json[0]);
            }).catch (err => alert(err));
        }, 500);
    });
};

export const runGASPARQL = (_gsid: string, _graph: string): void => {
    return new Promise ((resolve, reject) => {
        setTimeout(function () {
            let apiURL: string = `${glycanabundance.URL}?`;
            apiURL += `graph=${encodeURI(`http://glyconavi.org/${_graph}/`)}`;
            apiURL += `&id=${_gsid}`;

            fetch (apiURL).then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    alert("Request failed: " + res.status);
                }
            }).then(json => {
                resolve(json);
            }).catch(err => alert(err));
        }, 500);
    });
};

export const runPubMedAPI = (_gsid: string, _gsobj: Object) => {
    const pmid: string = _gsobj[_gsid].Reference;

    if (pmid === "") return [_gsobj, {"message": "Pubmed ID is not defined."}];

    // check for doi
    //https://www.crossref.org/blog/dois-and-matching-regular-expressions/
    //if (pmid.match("/^10.\d{4,9}/[-._;()/:A-Z0-9]+$/i")) {
    if (pmid.match(/^10\..+$/i)) {
        return new Promise((resolve, reject) => {
            setTimeout(function () {
                let apiURL: string = `${crossrefapi.URL}${pmid}`;

                fetch(apiURL).then(res => {
                    if (res.ok) {
                        return res.json();
                    } else {
                        alert(`Request failed: ${res.status}`);
                    }
                }).then(json => {
                    let ret: Object = {
                        uid: "",
                        pubdate: makePubDate(json.message.created["date-parts"][0]),
                        author: json.message.author,
                        title: json.message.title[0],
                        volume: json.message.volume,
                        issue: json.message.issue,
                        pages: json.message.page,
                        issn: json.message.ISSN[0],
                        essn: "",
                        articleids: [{
                            idtype: "doi",
                            value: json.message.DOI
                        }],
                        fulljournalname: json.message["short-container-title"][0],
                    };
                    resolve([_gsobj, ret]);
                }).catch(err => alert(err));
            }, 500);
        });
    } else {
        return new Promise((resolve, reject) => {
            setTimeout(function () {
                let apiURL: string = `${pubmedapi.URL}?id=${pmid}&db=pubmed&retmode=json`;

                fetch(apiURL).then(res => {
                    if (res.ok) {
                        return res.json();
                    } else {
                        alert(`Request failed: ${res.status}`);
                    }
                }).then(json => {
                    resolve([_gsobj, json.result[pmid]]);
                }).catch(err => alert(err));
            }, 500);
        });
    }
};

export const runDOIDAPI = (_gsid: string, _gsobj: Object) => {
    const doid: string = _gsobj[_gsid].DOID;

    if (doid === "") return [_gsobj, {"message": "DOID is not defined."}];

    return new Promise ( (resolve, reject) => {
        setTimeout (function () {
            let apiURL: string = diseaseOntology.URL + doid;

            fetch (apiURL).then (res => {
                if (res.ok) {
                    return res.json();
                } else {
                    alert(`Request failed: ${res.status}`);
                }
            }).then (json => {
                resolve([_gsobj]);
            }).catch (err => alert(err));
        }, 500);
    });
};

const makeError = (_err: string) => {
    let div: HTMLElement = document.getElementById("image");
    if (!div) {
        div = document.createElement("div");
        document.body.appendChild(div);
    }
    div.innerHTML = "";
    div.textContent = _err;
};

const makePubDate = (_pubdate: Array<string>) => {
    return `${_pubdate[0]} ${getNumberToMonth(_pubdate[1])}`;
};