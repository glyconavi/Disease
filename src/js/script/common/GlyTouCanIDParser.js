//@flow

"use strict";

import React from "react";

export const parseGlyTouCanID = (_gtcid: string): Array<string> => {
    const gtcID: Array<string> = _gtcid.match(/G[0-9]{5}[A-Z]{2}/g);
    let ret: Array<string> = [];
    if (gtcID !== null) {
        //return gtcID;
        gtcID.map( (id, index) => {
            ret.push(<div id={`gtcid_${gtcID}`} dangerouslySetInnerHTML={{__html: gtcID}} />);
        });
    } else {
        //return [""];
        ret.push(<div dangerouslySetInnerHTML={{__html: ""}} />);
    }
    return ret;
};