//@flow

"use strict";

import {dataLablePlugin} from "./dataLabel";
import * as ReactDOM from "react-dom";
import GroupUtilities from "../../component/group/GroupUtilities";
import React from "react";
import ChartLegend from "../../component/common/ChartLegend";

const Chart = require("chart.js");

export const makeGraph = (_diseaseGroup: Object, _diseaseElm: HTMLElement): HTMLElement => {
    const subsets: Object = makeSubsets(_diseaseGroup);
    const backGroundColor: Object = subsets.BackGroundColor;
    const projectDescription: Object = subsets.ProjectDescription;
    const glytoucanID: Object = _diseaseGroup.GlyTouCanID;
    delete subsets.BackGroundColor;
    delete subsets.ProjectDescription;

    for (let subKey: string of Object.keys(subsets)) {
        const unitWidth: number = 40 * subsets[subKey].label.length;
        const totalWidth: number = unitWidth * Object.keys(_diseaseGroup.SampleState).length;

        let elm: HTMLElement = document.createElement("div");
        elm.id = subKey;
        elm.style.border = "1px solid #ddd";
        elm.style.padding = "5px";
        elm.style.height = "auto";
        elm.style.textAlign = "center";
        elm.style.marginBottom = "4px";

        let canvas: HTMLElement = document.createElement("canvas");
        const title: string = makeTitle(_diseaseGroup);

        const datasets: Object = makeDataSets(subsets[subKey], projectDescription, backGroundColor, glytoucanID);

        let chart = new Chart(canvas.getContext("2d"), {
            type: "bar",
            data: datasets,
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: title + subKey,
                    fontSize: 16
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Samples",
                            fontSize: 16
                        },
                        ticks: {
                            fontSize: 14
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Abundance (%)",
                            fontSize: 16
                        },
                        ticks: {
                            suggestedMin: 0,
                            stepSize: 10,
                            fontSize: 14
                        }
                    }]
                },
                tooltips: {
                    mode: "label",
                    enabled: false
                },
                maintainAspectRatio: false
            },
            plugins: [dataLablePlugin]
            //
        });

        let graph: HTMLElement = document.createElement("div");
        graph.id = `graph_${subKey}`;
        graph.style.display = "flex";
        graph.style.flexDirection = "row";
        graph.style.justifyContent = "center";
        graph.style.backgroundColor = "rgba(255, 255, 255, 1)";

        let canvasTemp: HTMLElement = document.createElement("div");
        canvasTemp.id = `canvas_${subKey}`;
        canvasTemp.style.position = "relative";
        canvasTemp.style.height = "450px";
        canvasTemp.style.width = "75%";
        canvasTemp.appendChild(canvas);

        // make custom tooltip
        /*
        let tooltip: HTMLElement = document.createElement("div");
        tooltip.className = `tooltip_${subKey}`;
        elm.appendChild(tooltip);

        canvas.addEventListener("mousemove", (event) => {
            let offsetX: number = event.offsetX;
            let offsetY: number = event.offsetY;
            diseaseChart.scales["x-axis-0"]._labelItems.map( (item, index) => {
                let offset: number = item.textOffset;
                if (index === 0) {
                    if (item.rotation !== 0) {
                        offsetX = offsetX - item.font.lineHeight;
                        offsetY = offsetY - item.font.lineHeight;
                        offset = 12;
                    } else {
                        offsetY = offsetY - offset;
                    }
                }
                if (compareCoodinates(Math.floor(offsetX), Math.floor(item.x), offset * 2) &&
                    compareCoodinates(Math.floor(offsetY), Math.floor(item.y), offset * 2)) {
                    makeCustomToolTip(event, item, index, glytoucanID, subKey);
                }
            });
        });
        canvas.addEventListener("mouseout", removeToolTip(subKey));
         */

        graph.appendChild(canvasTemp);

        let legendTemp: HTMLElement = document.createElement("div");
        legendTemp.id = `${subKey}_legend`;
        ReactDOM.render(
            <ChartLegend chartopt={chart.options} legend={chart.legend} over={() => {}} out={() => {}} click={() => {}} />,
            legendTemp
        );

        legendTemp.align = "left";
        legendTemp.style.display = "flex";
        legendTemp.style.flexDirection = "column";
        legendTemp.style.marginTop = "10%";
        legendTemp.style.marginLeft = "5px";
        legendTemp.style.marginRight = "5px";
        legendTemp.style.wordWrap = "break-all";
        graph.appendChild(legendTemp);

        elm.appendChild(graph);

        // make sample images and relative abundance table
        let ret: HTMLElement = document.createElement("div");
        ret.id = `${subKey}_utilities`;
        ReactDOM.render(
            <GroupUtilities subset={subsets} subkey={subKey} gtcid={glytoucanID} description={projectDescription} />,
            ret
        );
        elm.appendChild(ret);

        _diseaseElm.appendChild(elm);
    }

    return _diseaseElm;
};

const makeSubsets = (_diseaseGroup: Object): Array<Object> => {
    const gp: Object = _diseaseGroup.GlycanProject;
    const sample: Object = _diseaseGroup.SampleState;
    const color: Object = _diseaseGroup.ColorScale;
    const subGroup: Object = _diseaseGroup.SubGroup;
    const order: Object = _diseaseGroup.Order;

    let ret: Object = {};
    let healthy: number = 0;
    let disease: number = 0;

    ret.ProjectDescription = {};
    ret.BackGroundColor = {};

    // merge project description
    for (let gpKey: string of Object.keys(gp)) {
        for (let subKey: string of Object.keys(gp[gpKey])) {
            if (subKey === "Title") continue;

            if (subKey === "ProjectDescription") {
                for (let gsLabel: string of Object.keys(gp[gpKey].ProjectDescription)) {
                    ret.ProjectDescription[gsLabel] = gp[gpKey].ProjectDescription[gsLabel];
                }
            }
        }
    }

    const sortedList: Array<string> = sortSamples(ret.ProjectDescription, sample);

    // make background color
    for (let gsKey: string of sortedList) {
        if (isDisease(gsKey, sample)) {
            disease = disease + 1;
            ret.BackGroundColor[gsKey] = defineColor("d", color.disease.alpha * disease);
        } else {
            healthy = healthy + 1;
            ret.BackGroundColor[gsKey] = defineColor("", color.healthy.alpha * healthy);
        }
    }

    // make column labels
    for (let subKey: string of Object.keys(subGroup)) {
        // make glycan sample labels
        if (ret[subKey] === undefined) {
            ret[subKey] = {
                label: order[subKey].split(". ")
            };
        }

        for (let gsKey: string of sortedList) {
            if (subGroup[subKey][gsKey] === undefined) continue;

            ret[subKey][gsKey] = {
                params: []
            };

            // make params
            for (let label: string of ret[subKey].label) {
                const param: string = subGroup[subKey][gsKey][label];
                ret[subKey][gsKey].params.push(param === "not detected" || param === "not provided" || param === undefined ? "0" : param);
            }
        }
    }

    return ret;
};

const defineColor = (_condition: string, _alpha: number): string => {
    if (_condition === "") {
        return `rgb(0, 0, 255, ${_alpha})`;
    } else {
        return `rgb(255, 0, 0, ${_alpha})`;
    }
};

const makeDataSets = (_dataSets: Object, _projectDescription: Object, _bgColor: Object, _gtcid: Object): Object => {
    let params: Array<Object> = [];
    let item: Object = {};

    for (let key: string of Object.keys(_dataSets)) {
        if (key === "label") continue;
        item = {
            label: _projectDescription[key],
            backgroundColor: _bgColor[key],
            data: _dataSets[key].params
        };
        params.push(item);
    }

    // make labels
    let labels: Array<string> = [];
    _dataSets.label.map((label, index) => {
        const relativelabel: string = _gtcid[label][0].replace("Glycan:", "");
        const gtcID: Array<string> = relativelabel.match(/G[0-9]{5}[A-Z]{2}/g);
        if (gtcID !== null) {
            labels.push(gtcID.join(", "));
        } else {
            labels.push(relativelabel);
        }
    });

    return {
        labels: labels,
        datasets: params
    };
};

const makeDataLabels = (_dataSets: Object): Array<string> => {
    let ret: Array<string> = [];
    _dataSets.label.map( (label, index) => {
        ret.push(`<div id={${index}}>${label}</div>`);
    });

    return ret;
};

const isDisease = (_gsKey: string, _sample: Object): boolean => {
    return (_sample[_gsKey].Disease !== "");
};

const makeTitle = (_diseaseGroup: Object): string => {
    let ret: string = "";
    for (let glycanproject: string in _diseaseGroup.GlycanProject) {
        ret = ret + glycanproject + ", ";
    }

    return ret;
};

const sortSamples = (_subGroup: Object, _sample: Object): Array<string> => {
    let controls: Array<string> = [];
    let disease: Array<string> = [];

    // extract controls
    for (let _sampleid: string of Object.keys(_subGroup)) {
        if (!isDisease(_sampleid, _sample)) {
            controls.push(_sampleid);
        } else {
            disease.push(_sampleid);
        }
    }

    return controls.concat(disease);
};

const compareCoodinates = (_q1, _q2, _offset): number => {
    const difference: number = Math.abs(_q1 - _q2);
    return (_offset > difference);
};

const removeToolTip = (_subKey: string) => {
    let tooltips: HTMLElement = document.getElementsByClassName(`tooltip_${_subKey}`);
    if (tooltips.length === 0) return;
    if (tooltips[0].children.length === 0) return;
    tooltips[0].children[0].remove();
};