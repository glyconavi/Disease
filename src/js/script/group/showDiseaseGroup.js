//@flow

"use strict";


import React from "react";
import {
    makeDiseaseGroupGraph,
    makeGlycanProject,
    makeGlycanSample,
    makeHeader
} from "./makeDetailContent";
import {runDGIDSPARQL} from "../common/apiUtilities";

export const showDiseaseGroup = (): void => {
    let dgid: string = document.select.dgList.value;
    showDiseaseGroupWithID(dgid);
};

export const showDiseaseGroupWithID = (_dgid: string): void => {
    Promise.resolve()
        .then(runDGIDSPARQL.bind(this, _dgid))
        .then( (value) => {
            parseDiseaseGroup(value, _dgid);
        });
};

const parseDiseaseGroup = (_dg: Object, _dgid: string): void => {
    let dgObj: Object = _dg[_dgid];
    let div: HTMLElement = document.getElementsByClassName("diseasegroup")[0];

    div.innerHTML = "";

    let diseaseGroup: HTMLElement = document.createElement("div");
    diseaseGroup.id = `dg_${_dgid}`;
    diseaseGroup.style.display = "flex";
    diseaseGroup.style.flexDirection = "column";
    diseaseGroup.style.alignItems = "center";

    // make header
    diseaseGroup = makeHeader(dgObj, diseaseGroup);

    // make graph
    diseaseGroup = makeDiseaseGroupGraph(dgObj, diseaseGroup);

    // make glycan project (GP) section
    diseaseGroup = makeGlycanProject(dgObj, diseaseGroup);

    // make glycan sample section
    diseaseGroup = makeGlycanSample(dgObj, diseaseGroup);


    div.appendChild(diseaseGroup);
};