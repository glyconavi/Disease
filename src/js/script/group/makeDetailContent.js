//@flow

"use strict";

import * as ReactDOM from "react-dom";
import GlycanProject from "../../component/group/GlycanProject";
import React from "react";
import GlycanSample from "../../component/group/GlycanSample";
import {makeGraph} from "./makeGraph";
import {pubmed} from "../../database/pubmed";

export const makeHeader = (dgObj: Object, _diseaseGroup: HTMLElement): HTMLElement => {
    let head: HTMLElement = document.createElement("h4");
    const groupTitle: string = dgObj.Title;
    const pmID: string = dgObj.Reference;
    head.innerHTML = `<div>${groupTitle}<br/><a href=${pubmed.URL}${pmID} target="_blank" >${pmID}</a></div>`;
    head.style.width = "80%";

    delete dgObj.Title;
    delete dgObj.Reference;
    _diseaseGroup.appendChild(head);
    return _diseaseGroup;
};

export const makeGlycanProject = (dgObj: Object, _diseaseGroup: HTMLElement): HTMLElement => {
    let gpComponent: HTMLElement = document.createElement("div");
    gpComponent.id = "details";
    gpComponent.style.marginBottom = "10px";
    gpComponent.style.width = "80%";

    let groupHead: HTMLElement = document.createElement("h4");
    groupHead.textContent = "Glycan Project";
    gpComponent.appendChild(groupHead);

    let list: HTMLElement = document.createElement("div");
    ReactDOM.render(
        <GlycanProject glycanproject={dgObj.GlycanProject} subgroup={dgObj.SubGroup} />,
        list
    );
    gpComponent.appendChild(list);

    _diseaseGroup.appendChild(gpComponent);
    return _diseaseGroup;
};

export const makeGlycanSample = (dgObj: Object, _diseaseGroup: HTMLElement): HTMLElement => {
    let gsComponent: HTMLElement = document.createElement("div");
    gsComponent.id = "sample";
    gsComponent.style.marginBottom = "10px";
    gsComponent.style.width = "80%";

    let sampleHead: HTMLElement = document.createElement("h4");
    sampleHead.innerText = "Glycan Sample";
    gsComponent.appendChild(sampleHead);

    let rows: Array<Object> = [];
    Object.keys(dgObj.GlycanProject).forEach((gpKey) => {
        Object.keys(dgObj.GlycanProject[gpKey].ProjectDescription).map((content) => {
            let item: Object = {
                "GlycanSampleID": content,
                "Description": dgObj.GlycanProject[gpKey].ProjectDescription[content]
            };
            rows.push(item);
        });
    });

    let list: HTMLElement = document.createElement("div");
    ReactDOM.render(
        <GlycanSample columns={[""]} rows={rows} />,
        list
    );
    gsComponent.appendChild(list);

    _diseaseGroup.appendChild(gsComponent);
    return _diseaseGroup;
};

export const makeDiseaseGroupGraph = (dgObj: Object, _diseaseGroup: HTMLElement) => {
    let gaComponent: HTMLElement = document.createElement("div");
    gaComponent.id = "abundance";
    gaComponent.style.marginBottom = "10px";
    gaComponent.style.width = "80%";

    let abundanceHead: HTMLElement = document.createElement("h4");
    abundanceHead.innerText = "Glycan Abundance Graph";
    gaComponent.appendChild(abundanceHead);

    gaComponent = makeGraph(dgObj, gaComponent);

    _diseaseGroup.appendChild(gaComponent);
    return _diseaseGroup;
};