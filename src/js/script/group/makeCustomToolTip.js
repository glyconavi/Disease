//@flow

"use strict";

import {strImage} from "../../database/glycanImage";

export const makeCustomToolTip = (event, item, index, _glytuocanID, _subkey) => {
    let tooltipClass: HTMLElement = document.getElementsByClassName(`tooltip_${_subkey}`)[0];

    let tooltipEl: HTMLElement = tooltipClass.children[0];
    let elmID: string = `tooltip_${_subkey}_${item.label}`;

    if (tooltipEl) {
        if (tooltipEl.id !== elmID) {
            tooltipClass.children[0].remove();
            tooltipEl.remove();
        }
    }

    // init element for rendering
    if (!tooltipEl) {
        tooltipEl = new Image();
        tooltipEl.id = elmID;
        tooltipEl.style.zIndex = 1;
        tooltipEl.style.border = "1px solid #666";
        tooltipClass.appendChild(tooltipEl);
    }

    // append glycan images
    makeImageSource(_glytuocanID[item.label][0]).forEach((imgsrc) => {
        tooltipEl.src = `${strImage.URL + imgsrc}`;
        tooltipEl.alt = "Missing Image";
    });

    // set tool tip position
    tooltipEl.style.position = "absolute";
    let mouseX: number = event.offsetX;
    let mouseY: number = event.pageY;
    tooltipEl.style.left = `${mouseX}px`;
    tooltipEl.style.top = `${mouseY}px`;
    tooltipEl.style.pointerEvents = "none";
    //tooltipEl.style.opacity = 1;
};

const makeImageSource = (_glycanLabel: string): Array<string> => {
    _glycanLabel = _glycanLabel.replace("Glycan:", "");
    let gtcid: Array<string> = _glycanLabel.match(/G[0-9]{5}[A-Z]{2}/g);
    if (gtcid !== null) {
        return gtcid;
    }

    let ret: Array<string> = [];
    _glycanLabel.split("&").forEach((label) => {
        ret.push(label);
    });

    return ret;
};
