//@flow

"use strict";

import {glycanImage} from "../../database/glycanImage";

/*
export const makeGlycanImages = (_subGroup: string, _dataSets: Object, _gtcID: Object): HTMLElement => {
    let legend: HTMLElement = document.createElement("div");
    legend.id = `${_subGroup}_img`;
    legend.style.display = "flex";
    legend.style.flexDirection = "row";
    legend.style.flexWrap = "wrap";
    legend.style.alignItems = "center";
    legend.style.justifyContent = "center";
    //legend.style.display = "none";

    if (Object.keys(_gtcID).length === 0) return legend;

    for (let sampleLabel: string of _dataSets[_subGroup].label) {
        if (isEmpty(_gtcID[sampleLabel])) continue;

        let sampleComponent: HTMLElement = document.createElement("div");
        let head: HTMLElement = document.createElement("h4");

        head.textContent = _gtcID[sampleLabel] !== undefined ? `${_gtcID[sampleLabel].join(", ")}` : `${sampleLabel}`;
        sampleComponent.id = `${sampleLabel}_comp`;
        sampleComponent.appendChild(head);

        for (let gtcID: string of _gtcID[sampleLabel]) {
            if (!gtcID.match(/^G\d{5}[A-Z]{2}$/)) continue;
            sampleComponent.appendChild(makeGlycanImageFromGTCID(gtcID));
        }
        legend.appendChild(sampleComponent);
    }

    return legend;
};

export const makeGlycanImageFromGTCID = (_gtcID: string): HTMLElement => {
    let imgComp: HTMLElement = document.createElement("div");
    imgComp.style.display = "inline-block";
    imgComp.id = _gtcID;

    if (!_gtcID.match(/^G\d{5}[A-Z]{2}$/)) return imgComp;

    let elm: HTMLElement = document.createElement("img");
    elm.src = makeImageSource(_gtcID);

    imgComp.appendChild(elm);

    return imgComp;
};

const isEmpty = (_gtcLegends: Array<string>): boolean => {
    if (_gtcLegends === undefined) return true;
    if (_gtcLegends.length === 1 && _gtcLegends.indexOf("") !== -1) {
        return true;
    }
    return (_gtcLegends.length === 0);
};
 */
