//@flow

"use strict";

export const dataLablePlugin = {
    afterDatasetsDraw: function(chart) {
        let ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i) {
            let meta = chart.getDatasetMeta(i);
            if (!meta.hidden) {
                meta.data.forEach(function(element, index) {

                    let dataString = dataset.data[index].toString();
                    let fontSize = 12;

                    //if (!isNaN(parseInt(dataString)) && parseInt(dataString) < 1) {
                    //    ctx.fillStyle = "red";
                    //} else {
                    ctx.fillStyle = "#666";
                    //}

                    let fontStyle = "normal";
                    let fontFamily = "Helvetica Neue";
                    ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                    ctx.textAlign = "center";
                    ctx.textBaseline = "middle";

                    let padding = 5;
                    let position = element.tooltipPosition();

                    ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                });
            }
        });
    }
};