//@flow

"use strict";

import React from "react";
import ReactDOM from "react-dom";
import KeyWordChips from "../../component/sample/KeyWordChips";
import {keyTips} from "./makeStaticalGraph";
import AbundanceTable from "../../component/abundance/AbundanceTable";
import {aggregatlist} from "../../parameters/aggregatelist";

export const makeSampleList = (_diseaseSample: Object): void => {
    // make chip set
    makeKeywordList(_diseaseSample);

    // make sample table
    sortSampleList(_diseaseSample);
};

const makeKeywordList = (_diseaseSample: Object): void => {
    let elm: HTMLElement = document.getElementById("keyword");

    if (!elm) {
        elm = document.createElement("div");
        elm.id = "keyword";
        document.getElementById("staticallist").appendChild(elm);
    }

    elm.classList.remove("above", "below", "no-transform");
    ReactDOM.render(
        <KeyWordChips chips={keyTips} sample={_diseaseSample} delete={ (keyword) => { deleteSelectedKeyword(keyword); }} sorttable={ () => { sortSampleList(_diseaseSample); }} />,
        elm
    );
};

export const sortSampleList = (_diseaseSample: Object): void => {
    let elm: HTMLElement = document.getElementById("list");

    if (!elm) {
        elm = document.createElement("div");
        elm.id = "list";
        document.getElementById("staticallist").appendChild(elm);
    }

    let rows: Array<Object> = [];
    let columns: Array<string> = ["GlycanSampleID"];

    // make columns
    Object.keys(aggregatlist).forEach( (key) => {
        if (key === "Taxon") return;
        columns.push(key);
    });

    // make rows
    Object.keys(_diseaseSample).forEach( (_dgid) => {
        let content: Object = {};
        let countKeyword: number = Object.keys(keyTips).length;

        columns.forEach( (_content) => {
            if (!_content in keyTips) return;
            let found: Array<string> = [];
            if (_content === "Sex") {
                const state: string = _diseaseSample[_dgid].Sex;
                if (state !== "") {
                    found = _diseaseSample[_dgid].Sex.match(/(fe)?male/g);
                }
            }

            if (found.length > 0) {
                if (found.length === 2 && keyTips[_content] === "both") {
                    countKeyword--;
                }
                if (found.length === 1 && found.indexOf(keyTips[_content]) !== -1) {
                    countKeyword--;
                }
            } else {
                if (_diseaseSample[_dgid][_content] === keyTips[_content]) countKeyword--;
            }

            content[_content] = _diseaseSample[_dgid][_content];
        });
        if (countKeyword !== 0) return;
        content.GraphType = _diseaseSample[_dgid].GraphType;
        rows.push(content);
    });

    elm.classList.remove("above", "below", "no-transform");
    ReactDOM.render(
        <AbundanceTable columns={columns} rows={rows} throw={["GraphType"]} />,
        elm
    );
};

export const setSekectedKeyword = (_title: string, _label: string): void => {
    keyTips[_title] = _label;
};

const deleteSelectedKeyword = (_keyword: string): void => {
    if (_keyword in keyTips === undefined) return;
    delete keyTips[_keyword];
};