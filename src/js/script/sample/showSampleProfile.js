//@flow

"use strict";

import ReactDOM from "react-dom";
import OpenWindowSampleProfile from "../../component/sample/OpenWindowSampleProfile";
import React from "react";
import GlycanSampleProfile from "../../component/sample/GlycanSampleProfile";

export const showGlycanSampleTableNewWindow = (_gsid: string, _result: Object, _journal: Object): void => {
    ReactDOM.render(
        <OpenWindowSampleProfile rows={makeGlycanSampleObject(_result, _gsid)} gsid={_gsid} journal={_journal} />,
        document.createElement("div")
    );
};

export const makeGlycanSampleTable = (_gsid: string, _result: Object, _journal: Object): void => {
    let elm: HTMLElement = document.getElementsByClassName(`gss_${_gsid}`)[0];

    ReactDOM.render(
        <GlycanSampleProfile rows={makeGlycanSampleObject(_result, _gsid)} gsid={_gsid} journal={_journal} />,
        elm
    );
};

const makeGlycanSampleObject = (_glycansample: Object, _gsid: string): Array<Object> => {
    let rows: Array<Object> = [];

    Object.keys(_glycansample[_gsid]).forEach( (key) => {
        if (typeof _glycansample[_gsid][key] === "object") return;
        if (key === "Reference" || key === "Date") return;
        let item: Object = {
            key: key,
            value: _glycansample[_gsid][key]
        };
        rows.push(item);
    });

    return rows;
};