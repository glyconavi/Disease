//@flow

"use strict";

import {aggregatlist} from "../../parameters/aggregatelist";
import "chartjs-plugin-colorschemes";
import {makeSampleList} from "./makeSampleList";
import React from "react";
import {runDSSPARQL} from "../common/apiUtilities";
import ReactDOM from "react-dom";
import ChartLegend from "../../component/common/ChartLegend";

const Chart = require("chart.js");
export let keyTips: Object = {};

export const showDiseaseSampleChart = () => {
    Promise.resolve()
        .then(runDSSPARQL.bind(this, "http://glyconavi.org/disease/"))
        .then( (value) => {
            //value[1]: GlycanSampleState
            makeVisualizationData(value[1]);
        });
};

const makeVisualizationData = (_diseaseSample: Object): void => {
    makeStaticalGraph(_diseaseSample);

    // make disease sample lists
    makeSampleList(_diseaseSample);
};

export const makeStaticalGraph = (_diseaseSample: Object): void => {
    const aggregatedData: Object = makeAggregatedData(_diseaseSample);

    const graphData: Object = makeDataSets(aggregatedData);

    makeGraph(graphData, _diseaseSample);
};

const makeAggregatedData = (_diseaseSample: Object): Object => {
    let ret: Object = {};

    for (let gsid of Object.keys(_diseaseSample)) {
        const sampleObj: Object = _diseaseSample[gsid];

        if (!countFilteredSample(sampleObj)) continue;

        for (let content: string of Object.keys(sampleObj)) {
            ret = countStatus(ret, content, sampleObj[content]);
        }
    }

    return ret;
};

const countStatus = (_index: Object, _key: string, _value: string): Object => {
    if (aggregatlist[_key] === undefined) return _index;
    if (_index[_key] === undefined) {
        _index[_key] = {};
    }

    if (_key === "Sex") {
        //if (_value === "") {
        //    _index[_key]["unknown"]  = _index[_key]["unknown"] === undefined ? 1 : _index[_key]["unknown"] + 1;
        //} else {
        const parsedSexSample: Array<string> = parseSex(_value);
        if (parsedSexSample === null) return _index;
        if (parsedSexSample.length === 2) {
            _index[_key].both = _index[_key].both === undefined ? 1 : _index[_key].both + 1;
        } else {
            const content: string = parsedSexSample[0];
            _index[_key][content] = _index[_key][content] === undefined ? 1 : _index[_key][content] + 1;
        }
        //}
        return _index;
    }

    if (_value === "") return _index;

    _index[_key][_value] = _index[_key][_value] === undefined ? 1 : _index[_key][_value] + 1;

    return _index;
};

const makeDataSets = (_aggregatedData: Object): Object => {
    let ret: Object = {};

    for (let caption: string of Object.keys(_aggregatedData)) {
        let item: Object = {
            labels:[],
            datasets: []
        };
        let dataset: Object = {
            borderWidth: 0,
            data: [],
            fill: false,
            hoverBackgroundColor: "#666",
        };

        for (let unitKey: string of Object.keys(_aggregatedData[caption])) {
            item.labels.push(unitKey);
            dataset.data.push(_aggregatedData[caption][unitKey]);
        }
        item.datasets.push(dataset);
        ret[caption] = item;
    }

    return ret;
};

const makeGraph = (_graphData: Object, _diseaseSample: Object): void => {
    let diseaseSample: HTMLElement = document.getElementById("staticalgraph");
    diseaseSample.style.display = "flex";
    diseaseSample.style.flexDirection = "column";

    diseaseSample.innerHTML = "";

    let component: HTMLElement = document.createElement("div");
    component.style.display = "flex";
    component.style.flexDirection = "row";
    component.style.justifyContent = "center";
    component.style.flexWrap = "wrap";

    Object.keys(_graphData).sort(sortContents).forEach( (category) => {
        let canvas: HTMLElement = document.createElement("canvas");
        canvas.id = "chart";

        let chart = new Chart(canvas.getContext("2d"), {
            type: "pie",
            data: _graphData[category],
            options: {
                legend: {
                    display: false,
                    position: "right",
                    align: "start"
                },
                title: {
                    display: true,
                    text: category,
                    fontSize: 16
                },
                onClick: function (event, elements) {
                    if (elements.length === 0) return;
                    const label: string = elements.length === 0 ? "" : elements[0]._model.label;
                    const title: string = elements[0]._chart.options.title.text;
                    if (label === "") return;

                    filterDiseaseSampleList(title, label, _diseaseSample);
                },
                plugins: {
                    colorschemes: {
                        scheme: "tableau.Classic20"
                    }
                },
                segmentShowStroke: false,
                maintainAspectRatio: false
            }
        });

        let graph: HTMLElement = document.createElement("div");
        graph.id = category;
        graph.style.backgroundColor = "rgba(255, 255, 255, 1)";
        graph.style.margin = "10px 5px 10px 5px";
        graph.style.display = "flex";
        graph.style.flexDirection = "row";
        graph.style.alignItems = "center";
        graph.style.justifyContent = "center";

        // canvas styling
        let canvasTemp: HTMLElement = document.createElement("div");
        canvasTemp.id = "canvas";
        canvasTemp.style.position = "relative";
        canvasTemp.style.width = "200px";
        canvasTemp.style.height = "200px";
        canvasTemp.appendChild(canvas);
        graph.appendChild(canvasTemp);

        let legendTemp: HTMLElement = document.createElement("div");
        legendTemp.id = `${chart.options.title.text}_legend`;
        ReactDOM.render(
            <ChartLegend chartopt={chart.options} legend={chart.legend} samples={_diseaseSample} over={mouseover} out={mouseout} click={click} />,
            legendTemp
        );

        legendTemp.align = "left";
        legendTemp.style.display = "flex";
        legendTemp.style.flexDirection = "column";
        legendTemp.style.height = "160px";
        legendTemp.style.margin = "30px 5px 0px 5px";
        legendTemp.style.overflowX = "hidden";
        legendTemp.style.width = "150px";
        legendTemp.style.wordWrap = "break-all";
        graph.appendChild(legendTemp);

        component.appendChild(graph);
    });

    diseaseSample.appendChild(component);
};

export const filterDiseaseSampleListWithEvent = (_e, _diseaseSample: Object): void => {
    const title: string = _e.currentTarget.className;
    const label: string = _e.currentTarget.childNodes[1].innerText;
    return filterDiseaseSampleList(title, label, _diseaseSample);
};

const filterDiseaseSampleList = (_title: string, _label: string, _diseaseSample: Object): void => {
    if (_title === "" || _label === "") return;
    keyTips[_title] = _label;
    //setSekectedKeyword(_title, _label);

    makeStaticalGraph(_diseaseSample);

    // make disease sample lists
    makeSampleList(_diseaseSample);
};

const countFilteredSample = (_diseasesample: Object): boolean => {
    let count: number = Object.keys(keyTips).length;
    Object.keys(_diseasesample).forEach( (content) => {
        if (!(content in keyTips)) return;
        if (content === "Sex") {
            const parsedSex: Array<string> = parseSex(_diseasesample.Sex);
            //if (_diseasesample.Sex === "" && keyTips[content] === "unknown") {
            //    count--;
            //}
            if (parsedSex === null) return;
            if (parsedSex.length === 2 && keyTips[content] === "both") {
                count--;
            }
            if (parsedSex.length === 1 && parsedSex.indexOf(keyTips[content]) !== -1) {
                count--;
            }
        } else {
            if (keyTips[content] === _diseasesample[content]) count--;
        }
    });
    return (count === 0);
};

const parseSex = (_content: string): Array<string> => {
    return _content.match(/(fe)?male/g);
};

const sortContents = (a, b) => {
    const aggregations: Array<string> = Object.keys(aggregatlist);
    let x = aggregations.indexOf(a);
    let y = aggregations.indexOf(b);
    if (x > y) return 1;
    if (x < y) return -1;
    return 0;
};

const mouseover = (e) => {
    e.currentTarget.childNodes[0].style.backgroundColor = "#666";
    e.currentTarget.childNodes[1].style.backgroundColor = "#666";
};

const mouseout = (_legendItem, e) => {
    e.currentTarget.childNodes[0].style.backgroundColor = _legendItem.fillStyle;
    e.currentTarget.childNodes[1].style.backgroundColor = "#fff";
};

const click = (_samples, e) => {
    filterDiseaseSampleListWithEvent(e, _samples);
};

window.filterDiseaseSampleList = filterDiseaseSampleList;