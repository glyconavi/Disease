//@flow

"use strict";

import React from "react";
import ReactDOM from "react-dom";
import AbundanceTable from "../../component/abundance/AbundanceTable";
import OpenWindowComponent from "../../component/abundance/OpenWindowComponent";
import DiseaseDBResources from "../../component/DiseaseDBResources";
import {runDGSPARQL, runDSSPARQL} from "../common/apiUtilities";
import {searchWord} from "../../main";

export const showDiseaseGroupList = () => {
    Promise.resolve()
        .then(runDGSPARQL.bind(this))
        .then( (value) => {
            //value: DiseaseAbundance
            makeAbundanceTable(value.result);
        });
};

// for test method
export const showDatabaseResourceis = () => {
    Promise.resolve()
        .then(runDGSPARQL.bind(this))
        .then(runDSSPARQL.bind(this, "http://glyconavi.org/disease/"))
        .then(runDSSPARQL.bind(this, "http://glyconavi.org/milksugar/"))
        .then(runDSSPARQL.bind(this, "http://glyconavi.org/other/"))
        //.then(runDSSPARQL.bind(this, "http://glyconavi.org/taxonomy/"))
        .then( (value) => {
            //value[0]: diseasegroup,
            //value[1]: disease glycan samples,
            //value[2]: milk sugar glycan samples,
            //value[3]: other glycan samples
            let disease: Object = value[1];
            let milkSugar: Object = value[2];
            let other: Object = value[3];
            const samples: Object = Object.assign(disease, milkSugar, other);
            makeAbundanceTableList(value[0].result, samples);
        });
};

const makeAbundanceTable = (_diseaseAbundance: Array<Object>): void => {
    //let abundance: HTMLElement = document.getElementById("diseaseabundance");
    let abundance: HTMLElement = document.getElementsByClassName("diseaseabundance")[0];

    let abundancelist: HTMLElement = document.createElement("div");
    abundancelist.id = "abundancelist";

    let header: HTMLElement = document.createElement("h4");
    header.innerText = "Disease Glycan Abundance Entries";
    //abundance.appendChild(header);

    //make columns
    let columns: Array<string> = [];
    Object.keys(_diseaseAbundance[0]).map((key) => {
        if (key === "GraphType") return;
        columns.push(key);
    });

    ReactDOM.render(
        <AbundanceTable columns={columns} rows={_diseaseAbundance} throw={["GraphType"]} />,
        abundancelist
    );
    abundance.appendChild(abundancelist);
};

// test utility
const makeAbundanceTableList = (_diseasegroup: Array<Object>, _glycansamples: Object): void => {
    let abundance: HTMLElement = document.getElementsByClassName("diseaseabundance")[0];
    let header: HTMLElement = document.createElement("h4");
    header.innerText = "Entries of Disease DB";
    abundance.appendChild(header);

    let abundanceblock: HTMLElement = document.createElement("div");
    abundanceblock.id = "abundanceblock";
    abundanceblock.style.textAlign = "center";

    ReactDOM.render(
        <DiseaseDBResources samples={_glycansamples} group={_diseasegroup} />,
        abundanceblock
    );
    abundance.appendChild(abundanceblock);
};

export const makeGlycanAbundanceTableNewWindow = (_gsid: string, _glycanSample: Array<Object>): void => {
    // make columns
    let columns: Array<string> = [];
    let throwList: Array<string> = ["GraphType"];
    Object.keys(_glycanSample[0]).forEach((content) => {
        if (content.indexOf(throwList) !== -1) return;
        content = content.replace("_", " ");
        columns.push(content);
    });

    ReactDOM.render(
        <OpenWindowComponent gsid={_gsid} columns={columns} rows={_glycanSample} throw={throwList} />,
        document.createElement("div")
    );
};

export const makeGlycanSampleAbundanceTable = (_gsid: string, _glycanSample: Array<Object>): void => {
    let gs: HTMLElement = document.getElementsByClassName(`gss_${_gsid}`)[0];
    let abundance: HTMLElement = document.createElement("div");
    abundance.id = `${_gsid}_abundance`;

    // make columns
    const items: Object = makeAbundanceColumns(_glycanSample);

    ReactDOM.render(
        <AbundanceTable columns={items.columns} rows={_glycanSample} throw={items.throws} />,
        abundance
    );

    // make header
    let header: HTMLElement = document.createElement("h4");
    header.innerText = "Glycan Abundance";
    abundance.prepend(header);

    gs.prepend(abundance);
};

export const makeGlycanAbundanceTable = (_gsid: string, _glycanSample: Array<Object>): void => {
    let gs: HTMLElement = document.getElementsByClassName(`gs_${_gsid}`)[0];
    let header: HTMLElement = document.createElement("h4");
    header.innerText = "Glyco Form";
    gs.appendChild(header);

    const items: Object = makeAbundanceColumns(_glycanSample);

    ReactDOM.render(
        <AbundanceTable columns={items.columns} rows={_glycanSample} throw={items.throws} />,
        gs
    );
};

export const setKeyWordFromSearchForm = (_searchKeyword): void => {
    _searchKeyword.split(" ").forEach( (word, index) => {
        searchWord[index] = word;
    });
};

export const sortAbundanceList = (_diseaseAbundance: Array<Object>): void => {

};

const makeAbundanceColumns = (_glycanSample: Array<Object>): Object => {
    // make columns
    let columns: Array<string> = [];
    let throwList: Array<string> = [];
    Object.keys(_glycanSample[0]).forEach((content) => {
        if (content === "Abundance_Name" || content === "Abundance_Param" || content === "ID" || content === "GraphType") {
            throwList.push(content);
            return;
        }
        content = content.replace("_", " ");
        columns.push(content);
    });

    return {columns: columns, throws: throwList};
};