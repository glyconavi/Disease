//@flow

"use strict";

import {
    makeGlycanAbundanceTable, makeGlycanAbundanceTableNewWindow, makeGlycanSampleAbundanceTable
} from "./makeAbundanceTable";
import {
    makeGlycanSampleTable, showGlycanSampleTableNewWindow,
} from "../sample/showSampleProfile";
import {runDOIDAPI, runGASPARQL, runGSIDSPARQL, runPubMedAPI} from "../common/apiUtilities";

export const showGlycanAbundanceEvent = (_e): void => {
    const gsid: string = _e.currentTarget.id;
    showGlycanAbundanceWithID(gsid);
};

export const showGlycanSampleEvent = (): void => {
    //const gsid: string = _e.currentTarget.id;
    showGlycanSampleWithID("GS_1", "disease");
};

export const showGlycanSampleWithID = (_gsid: string, _graph: string) => {
    Promise.resolve()
        .then(runGSIDSPARQL.bind(this, _gsid, _graph))
        .then(runPubMedAPI.bind(this, _gsid))
        //.then(runDOIDAPI.bind(this, _gsid))
        .then( (value) => {
            makeGlycanSampleTable(_gsid, value[0], value[1]);
        })
        .then(runGASPARQL.bind(this, _gsid, _graph))
        .then( (value) => {
            makeGlycanSampleAbundanceTable(_gsid, value);
        });
};

export const showGlycanAbundanceWithID = (_gsid: string, _graph: string) => {
    Promise.resolve()
        .then(runGASPARQL.bind(this, _gsid, _graph))
        .then( (value) => {
            makeGlycanAbundanceTable(_gsid, value);
        });
};

/* for debug components */
export const showGlycanSampleEventNewWindow = (_e): void => {
    const gsid: string = _e.currentTarget.id;
    const graph: string = _e.currentTarget.value;
    Promise.resolve()
        .then(runGSIDSPARQL.bind(this, gsid, graph))
        .then(runPubMedAPI.bind(this, gsid))
        //.then(runDOIDAPI.bind(this, gsid))
        .then( (value) => {
            // value[0]:
            showGlycanSampleTableNewWindow(gsid, value[0], value[1]);
        });
};

/* for debug components */
export const showGlycanAbundanceEventNewWindow = (_e): void => {
    const gsid: string = _e.currentTarget.id;
    const graph: string = _e.currentTarget.value;
    Promise.resolve()
        .then(runGASPARQL.bind(this, gsid, graph))
        .then( (value) => {
            makeGlycanAbundanceTableNewWindow(gsid, value);
        });
};
